#!/bin/bash

# seq -> FIRST INCREMENT LAST

for TTFW in 0.20 0.40 0.60 0.8 1.0 1.20 1.40 1.60 1.80 2.00
do
  for RIGHT_WINDOW_BORDER_X in 15.00 35.00 55.00 75.00 95.00 115.00 135.00 155.00 176.00 195.00 215.00 235.00 255.00 275.00 295.00 350.00 400.00
  do
    sed -i "s/TTFW=[0-9]\+\.[0-9]\+/TTFW=$TTFW/" script-spec-grid.sh;
    sed -i "s/RIGHT_WINDOW_BORDER_X=[0-9]\+\.[0-9]\+/RIGHT_WINDOW_BORDER_X=$RIGHT_WINDOW_BORDER_X/" script-spec-grid.sh;
    qsub script-spec-grid.sh;
    #cat script-spec-grid.sh;
  done
done
